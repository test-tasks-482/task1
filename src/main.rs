use std::collections::LinkedList;

fn sum_digit_by_digit(list1: &LinkedList<u8>, list2: &LinkedList<u8>) -> LinkedList<u8> {
    // Check just in case...
    if list1.is_empty() || list2.is_empty() || list1.len() > 100 || list2.len() > 100 ||
        list1.iter().any(|i| *i > 9u8) || list2.iter().any(|i| *i > 9u8) {
        panic!("Incorrect list range or bad content");
    }

    // Pick bigger to loop it
    let (bigger, smaller) = if list1.len() > list2.len() {
        (list1, list2)
    } else {
        (list2, list1)
    };
    let bigger = bigger.iter().rev();
    let mut smaller = smaller.iter().rev();

    // Move 1 to next
    let mut ten = false;

    let mut result = LinkedList::new(); // Here will be our result
    bigger.for_each(|a| {
        let mut b = match smaller.next() {
            Some(b) => *b,
            None => 0
        };
        b += *a;
        if ten {
            b += 1;
        }
        ten = b >= 10u8;
        result.push_front(b % 10);
    });
    // If it's last - we have add one to front
    if ten {
        result.push_front(1);
    }
    result
}

fn main() {
    let a = LinkedList::from([1, 2, 3]);
    let b = LinkedList::from([9, 1, 8]);
    let c = sum_digit_by_digit(&a, &b);
    print!("[");
    c.iter().for_each(|i| {
        print!("{},", i);
    });
    println!("]");
}

#[cfg(test)]
mod tests {
    use std::collections::LinkedList;

    #[test]
    fn test_sum() {
        let a = LinkedList::from([7, 2, 4, 3]);
        let b = LinkedList::from([5, 6, 4]);
        let c = crate::sum_digit_by_digit(&a, &b);

        assert_eq!(c, LinkedList::from([7, 8, 0, 7]));
    }

    #[test]
    #[should_panic]
    fn test_range_constraints_digit() {
        let a = LinkedList::from([1, 4, 11]);
        let b = LinkedList::from([2, 6]);
        let _ = crate::sum_digit_by_digit(&a, &b);
    }

    #[test]
    #[should_panic]
    fn test_range_constraints_max() {
        let a = LinkedList::from([5u8; 101]);
        let b = LinkedList::from([3u8; 4]);
        let _ = crate::sum_digit_by_digit(&a, &b);
    }

    #[test]
    #[should_panic]
    fn test_range_constraints_empty() {
        let a = LinkedList::from([5u8; 10]);
        let b = LinkedList::from([]);
        let _ = crate::sum_digit_by_digit(&a, &b);
    }
}
